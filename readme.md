# Spin up

```bash
docker compose --env-file .env.local up -d
```

# Spin down

```bash
docker compose --env-file .env.local down
```

> If there is no persmission to data files, check user id on container `id vault`, then change ownership of directories on docker side, using: `chown -R userId:groupId data/*`

# Server config template

```vault.hcl
ui            = true
cluster_addr  = "https://127.0.0.1:8201"
api_addr      = "https://127.0.0.1:8200"
disable_mlock = true

backend "file" {
  path = "/vault/data"
}

storage "raft" {
  path = "/path/to/raft/data"
  node_id = "raft_node_id"
}

listener "tcp" {
  address       = "127.0.0.1:8200"
  tls_cert_file = "/path/to/full-chain.pem"
  tls_key_file  = "/path/to/private-key.pem"
}

telemetry {
  statsite_address = "127.0.0.1:8125"
  disable_hostname = true
}
```